# README #

This is an executable launcher for launching Spotify and a Spotify AdBlocker on Windows at the same time.
The AdBlocker is configurable, and is minimised as soon as it is launched.
The source code and a compiled executable are in the repository.

Spotify is assumed to be already installed.

The executable is located at: *SpotifyAdBlockLauncher/bin/Release*

## INSTALLATION ##

There is also an installer for installing EZBlocker & the launcher itself, placing both parts in subdirectories of the default Spotify installation directory _C:\Users\%USERNAME%\AppData\Spotify_. The source code and compiled installer are both in the repository.

The installer is located at: *Installer/Release*, or from the downloads page.

## CONFIGURATION ##

The launcher is configurable by changing the config file located alongside it.
For example, to use a different AdBlocker, change the *AdBlockerLocation* setting value to point to the new executbale, and the *AdBlockerProcessName* value to the new process name.