﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;


namespace SpotifyAdBlockLauncher
{
    class Program
    {
        private const short Minimise = 2;

        [DllImport("user32.dll", SetLastError = true)]
        static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("user32.dll")]
        private static extern bool ShowWindowAsync(IntPtr hWnd, int nCmdShow);

        static void Main(string[] args)
        {
            /* Get the executable locations from the settings. */
            var spotifyLocation = Environment.ExpandEnvironmentVariables(Properties.Settings.Default.SpotifyLocation);
            var adBlockerLocation = Environment.ExpandEnvironmentVariables(Properties.Settings.Default.AdBlockerLocation);
            /* Start the processes if not already started */
            if (!IsProcessRunning(Properties.Settings.Default.SpotifyProcessName))
            {
            Process.Start(spotifyLocation);
                
            }
            if (!IsProcessRunning(Properties.Settings.Default.AdBlockerProcessName))
            {
                var process = Process.Start(adBlockerLocation);
                /* Minimise the ad blocker - specifically any window with that name. */
                if (process != null)
                {
                    Thread.Sleep(Properties.Settings.Default.WindowMinimiseWaitTime);
                    var handle = FindWindow(null, process.MainWindowTitle);
                    ShowWindowAsync(handle, Minimise);
                }
            }

        }

        private static Boolean IsProcessRunning(string processName)
        {
            return Process.GetProcessesByName(processName).Length != 0;
        }
    }
}
